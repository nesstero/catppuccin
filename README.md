# Catppuccin
[Soothing pastel theme for the high-spirited!](https://github.com/catppuccin)

- Catppuccin for [st](https://st.suckless.org/)
- Catppuccin for [qutebrowser](https://qutebrowser.org/)
- catppuccin for [rofi](https://github.com/davatorium/rofi)
- catppuccin for [qt5ct](https://sourceforge.net/projects/qt5ct/)

# Preview
- st
![st](ss/st.png)
- qutebrowser
![qb](ss/qb.png)
- rofi
![rofi](ss/rofi.png)
- qt5ct
![qt5ct](ss/qt5ct.png)
