# Activating theme
- Move folder **catppuccin** inside the configuration directory
- In your qutebrowser config.py file, include the following:
```python
from catppuccin import theme
theme.tampilan(c)
```
